rational: main.o rational.o
	g++ -std=c++11 main.o rational.o -o rational

main.o: main.cpp
	g++ -std=c++11 -c main.cpp

rational.o: rational.cpp rational.h
	g++ -std=c++11 -c rational.cpp

clean:
	rm *.o